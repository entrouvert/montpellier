<?php

$metadata['https://connexion.montpellier3m.fr/idp/saml2/metadata'] = array (
  'entityid' => 'https://connexion.montpellier3m.fr/idp/saml2/metadata',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://connexion.montpellier3m.fr/idp/saml2/sso',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://connexion.montpellier3m.fr/idp/saml2/sso',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://connexion.montpellier3m.fr/idp/saml2/slo',
      'ResponseLocation' => 'https://connexion.montpellier3m.fr/idp/saml2/slo_return',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://connexion.montpellier3m.fr/idp/saml2/slo',
      'ResponseLocation' => 'https://connexion.montpellier3m.fr/idp/saml2/slo_return',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://connexion.montpellier3m.fr/idp/saml2/slo/soap',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://connexion.montpellier3m.fr/idp/saml2/artifact',
      'index' => 0,
    ),
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIC+TCCAeGgAwIBAgIJAKiAR+I7dFTmMA0GCSqGSIb3DQEBBQUAMBMxETAPBgNV
BAMMCHdob2NhcmVzMB4XDTE0MTEyNjEyMDQzOFoXDTI0MTEyMzEyMDQzOFowEzER
MA8GA1UEAwwId2hvY2FyZXMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB
AQC50uhcC0e8B6+0wu7UaMs95h6bD7o6MqJnvTwkQ9OteyIK+qgXtSS5dKZCnY6r
fHKV6JVCpHrjc5eR0M1vZjCLRfbJzlQaLnatHibpTswsnAsCikXvNhrxuHConqX9
vRHxI6mcH1QydImSQALJDtYVI4qB9yy2gBbudxc2Q01KxT5+Q4ODNDHKOPtBtnZA
hKtVu+HwjOvUPeNUhcHdt+LORm09Cvgn/d7iD6qIWMkYYE3ec98KE5t93jupG7Bg
HAi8aDScfKxyTS1r/KADVK2mdStSNrnxHRt/wHGyYsR4XZRAfsdABp+jej3PMBe+
SmbCXgK6iyVBoyHoYrdyc9v7AgMBAAGjUDBOMB0GA1UdDgQWBBRnTNWO0ySFUJrR
zgMlKT08BCEe3TAfBgNVHSMEGDAWgBRnTNWO0ySFUJrRzgMlKT08BCEe3TAMBgNV
HRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQBa/v8yNYLylg8X2iesUZuJLG2K
BLSYzaU2yfB2FX7upb467uyblb/Ekxv3WV+mEHLOIJnM31B60HmQg3VQLymS+bBX
nR+T1n+b2W5DtTUeDdqdF8wmAbO30tgTxDD7xkF/YjZFDxUUc+RyTLMDsxq1jMDy
E6WRHf+tPp/7ZqW1WlBIs2WoXfVOmC7c674w3rjEV4nrZq63uzQJm0q7e0eyH7YR
yodHJ8dHb0FNbMebrzFdE7Avootb9kySIZ2GqDiovxQ57fngVdJPiqzyPHItJszg
UbDyAtIG3MT92BbW2iXLT1l56zp5kjTfhDexIyo8T1xrsQA+dCNXK/nD72IA',
    ),
  ),
);

$metadata['https://connexion.montpellier-agglo.com/idp/saml2/metadata'] = array (
  'entityid' => 'https://connexion.montpellier-agglo.com/idp/saml2/metadata',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://connexion.montpellier-agglo.com/idp/saml2/sso',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://connexion.montpellier-agglo.com/idp/saml2/sso',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://connexion.montpellier-agglo.com/idp/saml2/slo',
      'ResponseLocation' => 'https://connexion.montpellier-agglo.com/idp/saml2/slo_return',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://connexion.montpellier-agglo.com/idp/saml2/slo',
      'ResponseLocation' => 'https://connexion.montpellier-agglo.com/idp/saml2/slo_return',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://connexion.montpellier-agglo.com/idp/saml2/slo/soap',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://connexion.montpellier-agglo.com/idp/saml2/artifact',
      'index' => 0,
    ),
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIC+TCCAeGgAwIBAgIJAKiAR+I7dFTmMA0GCSqGSIb3DQEBBQUAMBMxETAPBgNV
BAMMCHdob2NhcmVzMB4XDTE0MTEyNjEyMDQzOFoXDTI0MTEyMzEyMDQzOFowEzER
MA8GA1UEAwwId2hvY2FyZXMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB
AQC50uhcC0e8B6+0wu7UaMs95h6bD7o6MqJnvTwkQ9OteyIK+qgXtSS5dKZCnY6r
fHKV6JVCpHrjc5eR0M1vZjCLRfbJzlQaLnatHibpTswsnAsCikXvNhrxuHConqX9
vRHxI6mcH1QydImSQALJDtYVI4qB9yy2gBbudxc2Q01KxT5+Q4ODNDHKOPtBtnZA
hKtVu+HwjOvUPeNUhcHdt+LORm09Cvgn/d7iD6qIWMkYYE3ec98KE5t93jupG7Bg
HAi8aDScfKxyTS1r/KADVK2mdStSNrnxHRt/wHGyYsR4XZRAfsdABp+jej3PMBe+
SmbCXgK6iyVBoyHoYrdyc9v7AgMBAAGjUDBOMB0GA1UdDgQWBBRnTNWO0ySFUJrR
zgMlKT08BCEe3TAfBgNVHSMEGDAWgBRnTNWO0ySFUJrRzgMlKT08BCEe3TAMBgNV
HRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQBa/v8yNYLylg8X2iesUZuJLG2K
BLSYzaU2yfB2FX7upb467uyblb/Ekxv3WV+mEHLOIJnM31B60HmQg3VQLymS+bBX
nR+T1n+b2W5DtTUeDdqdF8wmAbO30tgTxDD7xkF/YjZFDxUUc+RyTLMDsxq1jMDy
E6WRHf+tPp/7ZqW1WlBIs2WoXfVOmC7c674w3rjEV4nrZq63uzQJm0q7e0eyH7YR
yodHJ8dHb0FNbMebrzFdE7Avootb9kySIZ2GqDiovxQ57fngVdJPiqzyPHItJszg
UbDyAtIG3MT92BbW2iXLT1l56zp5kjTfhDexIyo8T1xrsQA+dCNXK/nD72IA',
    ),
  ),
);

$metadata['https://idp-test-entrouvert.montpellier3m.fr/idp/saml2/metadata'] = array (
  'entityid' => 'https://idp-test-entrouvert.montpellier3m.fr/idp/saml2/metadata',
  'contacts' => 
  array (
  ),
  'metadata-set' => 'saml20-idp-remote',
  'SingleSignOnService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp-test-entrouvert.montpellier3m.fr/idp/saml2/sso',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp-test-entrouvert.montpellier3m.fr/idp/saml2/sso',
    ),
  ),
  'SingleLogoutService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect',
      'Location' => 'https://idp-test-entrouvert.montpellier3m.fr/idp/saml2/slo',
      'ResponseLocation' => 'https://idp-test-entrouvert.montpellier3m.fr/idp/saml2/slo_return',
    ),
    1 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST',
      'Location' => 'https://idp-test-entrouvert.montpellier3m.fr/idp/saml2/slo',
      'ResponseLocation' => 'https://idp-test-entrouvert.montpellier3m.fr/idp/saml2/slo_return',
    ),
    2 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp-test-entrouvert.montpellier3m.fr/idp/saml2/slo/soap',
    ),
  ),
  'ArtifactResolutionService' => 
  array (
    0 => 
    array (
      'Binding' => 'urn:oasis:names:tc:SAML:2.0:bindings:SOAP',
      'Location' => 'https://idp-test-entrouvert.montpellier3m.fr/idp/saml2/artifact',
      'index' => 0,
    ),
  ),
  'keys' => 
  array (
    0 => 
    array (
      'encryption' => true,
      'signing' => true,
      'type' => 'X509Certificate',
      'X509Certificate' => 'MIIEFzCCAv+gAwIBAgIJAO8ugUMlPrlVMA0GCSqGSIb3DQEBBQUAMIGhMQswCQYD
VQQGEwJGUjEPMA0GA1UECAwGRnJhbmNlMQ4wDAYDVQQHDAVQYXJpczEUMBIGA1UE
CgwLRW50cidvdXZlcnQxMjAwBgNVBAMMKWlkcC10ZXN0LWVudHJvdXZlcnQubW9u
dHBlbGxpZXItYWdnbG8uY29tMScwJQYJKoZIhvcNAQkBFhh3ZWJtYXN0ZXJAZW50
cm91ZXZydC5jb20wHhcNMTMwNjE5MTIwNDI2WhcNMjMwNjE5MTIwNDI2WjCBoTEL
MAkGA1UEBhMCRlIxDzANBgNVBAgMBkZyYW5jZTEOMAwGA1UEBwwFUGFyaXMxFDAS
BgNVBAoMC0VudHInb3V2ZXJ0MTIwMAYDVQQDDClpZHAtdGVzdC1lbnRyb3V2ZXJ0
Lm1vbnRwZWxsaWVyLWFnZ2xvLmNvbTEnMCUGCSqGSIb3DQEJARYYd2VibWFzdGVy
QGVudHJvdWV2cnQuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA
n74TJeidFPecL7WWxhl/VweYVgiKt1rkwqsM4bRy8pS+g/7IgeJBdJXiNQzxYHi+
uW/9bEJN96unh4ug19zkjcRskGjJV5bDKmk0XiX8l6uAO8X2hEr7VOgOrdUd1McY
MOohUhwQqydrKquIvKZOKyQ2O44+afGZCpYlg0TWjiab8iDknYn05cHDCtC4kHxd
BotPnfFmhcfuV9zK2UNekfuY4SO2vbtM4qWO4O1OowBoW/qSW7C4/Oz10g3JUGbT
H2ryeXPt15huUZ/RymPk4NmDKeW7S3oJbxFHrsFw9zbed26puBZuTDRABH9TWZcK
I98D5VkOHOqbURu4V2WMaQIDAQABo1AwTjAdBgNVHQ4EFgQUQ7XR5BjeXZDeFfay
LJXc1GWMiTAwHwYDVR0jBBgwFoAUQ7XR5BjeXZDeFfayLJXc1GWMiTAwDAYDVR0T
BAUwAwEB/zANBgkqhkiG9w0BAQUFAAOCAQEAU/mCHGXP1GTGhyoxoRBSAoD3LWox
+RTh7eksMaiLMSYtbzjxntkO6KtE2dNbxMIwf0BRTJi/A+UoXsjyA5kmUE2xnHNN
gepTwvJEy2GoPeGgZNxeMoTZ/1I22kC1vGmfqEGmh8ZqIePgrZEQdydsD8lOgt6x
bVh9bXf5aD18m/DW2wbtdpq5UistGarsRePFsHotWm0OruPvpjIr25G5Myzz1vW+
iexrTkIFZoFsmfHkEZPqhCuoyokSF/CxngYiMm6aLzgYZoQIsT8VQwbpPPXZKS9L
YsNo7sQaeTJNPlQ0ELi7XW/O1gdl3rkqBAtZusmBZQaYN9brS8III+Z1ZA==',
    ),
  ),
);

